# Siemens IoT 2000
## Workshop vom 11.5.17 (goo.gl/iNQ4dW)  

Komplettes Skript inkl. Workshop-Exercises [Skript [PDF]](https://gitlab.com/alainrohr/iot2000/blob/master/iot2000.pdf) 

Einsatz in der Lehre/Trends IoT [The Big Picture [PDF]](http://w3.siemens.com/mcms/sce/de/sce-news/tabcardseiten/Documents/2017-03-Final_Plakat_fuer_AALE-Konferenz_2017-Wildau_V2.pdf)

![Picture 2020](http://w3.siemens.com/mcms/pc-based-automation/de/industrial-iot/PublishingImages/iot2000-2040.jpg)

### Siemens IoT2000-Devices
- [Unboxing IoT2020](https://www.rs-online.com/designspark/siemens-iot2020---unboxing-and-technical-look)
- [Siemens IoT2020/2040 Manual](https://support.industry.siemens.com/cs/document/109741658/simatic-iot2020-simatic-iot2040?dti=0&lc=de-CH)
- [Startup / SD-Card Image](https://support.industry.siemens.com/cs/document/109741799/simatic-iot2000-sd-card-beispielimage?dti=0&lc=de-CH)
- [Siemens IoT2020 IO, Input/Output Module](https://support.industry.siemens.com/cs/document/109745681/iot2000-io-input-output-module?dti=0&lc=de-CH)
- [Siemens S7 Node](file:///C:/temp/S7_Comunication_node-red_V1.0.pdf)
- [LoRaWan mit Iot2020](https://www.rs-online.com/designspark/python-and-lorawan-enabling-the-siemens-iot2020)

### Node-Red
- [Hauptseite](https://nodered.org/)
- [Node-Red Dashboard](https://www.rs-online.com/designspark/python-and-lorawan-enabling-the-siemens-iot2020)

### Mqtt
- [Theorie](https://www.heise.de/developer/artikel/Kommunikation-ueber-MQTT-3238975.html)
- [Client Tools](http://www.hivemq.com/blog/seven-best-mqtt-client-tools)
- [Android Client](https://play.google.com/store/apps/details?id=net.routix.mqttdash&hl=de)

### Workshop
| Aufgabe | Lösung |
| ------ | ------ |
| Einfache Input/Output-Knoten | [Lösung 1.1](https://gitlab.com/alainrohr/iot2000/blob/master/workshop/Sol_Bsp_1_1.txt)|
| MQTT Kommunikation | [Lösung 1.2](https://gitlab.com/alainrohr/iot2000/blob/master/workshop/Sol_Bsp_1_2.txt) |
| Bluemix Cloud Anbindung | [Lösung 1.3](https://gitlab.com/alainrohr/iot2000/blob/master/workshop/Sol_Bsp_1_3.txt) |
| HTTP Requests | [Lösung 2.1](https://gitlab.com/alainrohr/iot2000/blob/master/workshop/Sol_Bsp_2_1.txt) |
| WebServices(REST/JSON) | [Lösung 2.2](https://gitlab.com/alainrohr/iot2000/blob/master/workshop/Sol_Bsp_2_2.txt) |
| Eigene Knoten mit Javascript | [Lösung 2.3](https://gitlab.com/alainrohr/iot2000/blob/master/workshop/Sol_Bsp_2_3.txt) |